#!/bin/sh

PACKAGE=$1

GITSTATUS=$(git status --porcelain)
if [ ! -z "$GITSTATUS" ]; then
	git add .

	VERSION=$(.ci/scripts/get-version.sh "${PACKAGE}");
	MESSAGE="Incremented to version $VERSION"

	git checkout $CI_COMMIT_REF_NAME
	git commit -m "[SKIP CI] $MESSAGE"
	git tag -a "$VERSION" -m "$MESSAGE"
	git push --follow-tags origin $CI_COMMIT_REF_NAME
fi