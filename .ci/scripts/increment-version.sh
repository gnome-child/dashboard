#!/bin/sh

COMMITLEVEL=false
case ${CI_COMMIT_TITLE} in
  *\[MAJOR\]*)
    COMMITLEVEL="major"
    ;;
  *\[MINOR\]*)
    COMMITLEVEL="minor"
    ;;
  *\[PATCH\]*)
    COMMITLEVEL="patch"
    ;;
esac

if [ $COMMITLEVEL != false ]; then
  if [ -z $1 ]; then
    npm version $COMMITLEVEL --git-tag-version=false
  else
    mv $1 package.json
    npm version $COMMITLEVEL --git-tag-version=false
    mv package.json $1
  fi
fi
