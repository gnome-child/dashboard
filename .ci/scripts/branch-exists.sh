#!/bin/sh

REPO=$1
BRANCH=$2

exists=`git ls-remote --heads ${REPO} ${BRANCH} | wc -l`

echo $exists