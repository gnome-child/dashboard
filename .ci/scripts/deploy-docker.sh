#!/bin/sh

DOCKER_REPO=$1
TYPE=$2
PACKAGEFILE=$3

TAG="${CI_COMMIT_REF_SLUG}"
PUSHVERSION=false

docker login -u $HUB_USERNAME -p $HUB_PASSWORD

if [ $TAG == "master" ]; then
	TAG='latest'
fi

if [ $TAG == "latest" ]; then 
	PUSHVERSION=true
fi

if [ $TYPE == "ONLY_BRANCH" ] || [ $TYPE == 'BOTH' ]; then
	docker tag $DOCKER_REPO "jurienhamaker/${DOCKER_REPO}:${TAG}"
	docker push "jurienhamaker/${DOCKER_REPO}:${TAG}"
fi

VERSION=$(.ci/scripts/get-version.sh $PACKAGEFILE);
if [ $TYPE == "ONLY_VERSION" ] || [ $TYPE == 'BOTH' ]; then
    if [ $VERSION ]; then
		if [ $PUSHVERSION == true ]; then
			docker tag $DOCKER_REPO "jurienhamaker/${DOCKER_REPO}:${VERSION}"
			docker push "jurienhamaker/${DOCKER_REPO}:${VERSION}"
		else
			docker tag $DOCKER_REPO "jurienhamaker/${DOCKER_REPO}:${TAG}-${VERSION}"
			docker push "jurienhamaker/${DOCKER_REPO}:${TAG}-${VERSION}"	
		fi
    fi
fi