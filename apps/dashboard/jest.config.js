module.exports = {
  name: 'gnome-child',
  preset: '../../jest.config.js',
  coverageDirectory: '../../coverage/apps/gnome-child',
  snapshotSerializers: [
    'jest-preset-angular/AngularSnapshotSerializer.js',
    'jest-preset-angular/HTMLCommentSerializer.js'
  ]
};
