import { NestFactory } from '@nestjs/core';

import { AppModule } from './app/app.module';
import { SwaggerModule, DocumentBuilder } from '@nestjs/swagger';
import { environment } from './environments/environment';

async function bootstrap() {
	const app = await NestFactory.create(AppModule);

	if (!environment.production) {
		const options = new DocumentBuilder()
			.setTitle('Gnome-Child API')
			.setDescription('The Gnome-Child Discord Bot API description')
			.setVersion('1.0')
			.addBearerAuth()
			.build();
		const document = SwaggerModule.createDocument(app, options);
		SwaggerModule.setup('', app, document);
		app.use('/openapi.json', (req, res, next) => res.send(document));
	}

	const port = process.env.port || 3000;
	await app.listen(port, () => {
		console.log(`Listening at http://localhost:${port}/`);
	});
}

bootstrap();
