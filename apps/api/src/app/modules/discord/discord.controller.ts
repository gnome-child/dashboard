import { Controller, Get, UseGuards, Req } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { DiscordService } from './discord.service';
import { Request } from 'express';
import { DiscordMe } from './discord.interfaces';

import { ApiUseTags, ApiBearerAuth, ApiOperation, ApiOkResponse } from '@nestjs/swagger';

@ApiBearerAuth()
@ApiUseTags('discord')
@Controller('discord')
export class DiscordController {
	constructor(private _discordService: DiscordService) {}

	@Get('')
	@UseGuards(AuthGuard('jwt'))
	@ApiOperation({ title: "Discord information of the current logged in user." })
	@ApiOkResponse({ type: DiscordMe })
	me(@Req() request: Request) {
		return this._discordService.me(request.user.accessToken);
	}
}