import { ApiModelProperty } from '@nestjs/swagger';

export class DiscordMe {
	@ApiModelProperty()
	readonly username: string;

	@ApiModelProperty()
	readonly verified: boolean;

	@ApiModelProperty()
	readonly locale: string;

	@ApiModelProperty()
	readonly premium_type: number;

	@ApiModelProperty()
	readonly mfa_enabled: boolean;

	@ApiModelProperty()
	readonly id: string;

	@ApiModelProperty()
	readonly flags: number;

	@ApiModelProperty()
	readonly avatar: string;

	@ApiModelProperty()
	readonly discriminator: string;

	@ApiModelProperty({
		required: false
	})
	readonly email?: string;
}