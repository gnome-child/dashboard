import { Injectable, HttpService, HttpException } from '@nestjs/common';
import { discordConfig } from '../../config/discord';
import { Request } from 'express';
import { catchError, map } from 'rxjs/operators';
import { environment } from '../../../environments/environment.prod';


@Injectable()
export class DiscordService {
	constructor(private _httpService: HttpService) {};

	private _prepare(accessToken: Request): any {
		return {
			headers: {
				Authorization: `Bearer ${accessToken}`
			}
		};
	}

	async me(accessToken: string) {
		const config = this._prepare(accessToken);
		return this._httpService
			.request({
				...config,
				baseURL: discordConfig.apiUrl,
				url: discordConfig.endPoints.user.me,
				method: 'get'
			})
			.pipe(
				catchError(err => {
					console.log(err);
					throw new HttpException(err.response.statusText, err.response.status);
				}),
				map(response => response.data)
			);
	}
}