import { Module, HttpModule } from '@nestjs/common';
import { DiscordService } from './discord.service';
import { DiscordController } from './discord.controller';

@Module({
	imports: [
		HttpModule.register({
			timeout: 5000,
			maxRedirects: 5
		})
	],
	controllers: [DiscordController],
	providers: [
		DiscordService
	]
})
export class DiscordModule {}