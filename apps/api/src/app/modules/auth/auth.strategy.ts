import { Injectable } from "@nestjs/common";
import { PassportStrategy } from "@nestjs/passport";
import { Strategy } from "passport-discord-oauth2";
import { environment } from '../../../environments/environment';
import { AuthService, Provider } from './auth.service';


@Injectable()
export class AuthStrategy extends PassportStrategy(Strategy, 'discord')
{
	
	constructor(private _authService: AuthService) {
		super({
			clientID    : environment.auth.clientId,     // <- Replace this with your client id
			clientSecret: environment.auth.clientSecret, // <- Replace this with your client secret
			callbackURL : `${environment.host}/auth/callback`,
			scope: ['identify', 'email', 'guilds']
		})
	}

	async validate(accessToken: string, refreshToken: string, profile, done: Function) {
		try
		{
			const jwt: string = await this._authService.validateOAuthLogin(accessToken, refreshToken, Provider.DISCORD);
			const user =  {
				jwt
			};

			done(null, user);
		}
		catch(err)
		{
			// console.log(err)
			done(err, false);
		}
	}

}