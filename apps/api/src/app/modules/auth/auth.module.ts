import { Module } from '@nestjs/common';
import { AuthController } from './auth.controller';
import { AuthService } from './auth.service';
import { AuthStrategy } from './auth.strategy';
import { JwtStrategy } from './jwt.strategy';

@Module({
	controllers: [AuthController],
	providers: [
		AuthService,
		AuthStrategy,
		JwtStrategy
	]
})
export class AuthModule {}