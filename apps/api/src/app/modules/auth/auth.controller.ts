import { Controller, Get, UseGuards, Res, Req } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { environment } from '../../../environments/environment';
import { ApiUseTags } from '@nestjs/swagger';

@ApiUseTags('auth')
@Controller('auth')
export class AuthController {

	@Get('')
	@UseGuards(AuthGuard('discord'))
	login() {}

	@Get('callback')
	@UseGuards(AuthGuard('discord'))
	callback(@Req() req, @Res() res) {
		// handles the Google OAuth2 callback
		const jwt: string = req.user.jwt;
		if (jwt) {
			res.redirect(`${environment.dashboardUrl}/login/succes/${jwt}`);
		}
			
		else {
			res.redirect(`${environment.dashboardUrl}/login/failure`);
		}
	}
}