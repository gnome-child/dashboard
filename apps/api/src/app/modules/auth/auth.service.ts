import { Injectable, InternalServerErrorException } from '@nestjs/common';
import { sign } from 'jsonwebtoken';
import { environment } from '../../../environments/environment';

export enum Provider
{
	DISCORD = 'discord'
}

@Injectable()
export class AuthService {
	constructor(/*private readonly usersService: UsersService*/) {
	};

	async validateOAuthLogin(accessToken: string, refreshToken: string, provider: Provider): Promise<string> {
		try 
		{
			// You can add some registration logic here, 
			// to register the user using their thirdPartyId (in this case their googleId)
			// let user: IUser = await this.usersService.findOneByThirdPartyId(thirdPartyId, provider);
			
			// if (!user)
				// user = await this.usersService.registerOAuthUser(thirdPartyId, provider);
				
			const payload = {
				accessToken,
				refreshToken,
				provider
			};

			const jwt: string = sign(payload, environment.jwtSecret, { expiresIn: 3600 });
			return jwt;
		}
		catch (err)
		{
			throw new InternalServerErrorException('validateOAuthLogin', err.message);
		}
	}

}