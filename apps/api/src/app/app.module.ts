import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';

import { environment } from '../environments/environment';
import { join } from 'path';
import { AuthModule } from './modules/auth/auth.module';
import { DiscordModule } from './modules/discord/discord.module';

@Module({
	imports: [
		TypeOrmModule.forRoot({
			type: 'mongodb',
			host: environment.db.host,
			entities: [join(__dirname, '**/**.entity{.ts,.js}')],
			synchronize: true,
			useNewUrlParser: true,
			logging: true
		}),

		AuthModule,
		DiscordModule
	],
	providers: []
})
export class AppModule {}
