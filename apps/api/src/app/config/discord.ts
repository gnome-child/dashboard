export const discordConfig = {
	apiUrl: 'https://discordapp.com/api/',
	endPoints: {
		user: {
			me: 'users/@me'
		}
	}
}