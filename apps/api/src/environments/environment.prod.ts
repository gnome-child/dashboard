export const environment = {
	production: true,
	version: '1.0.0',
	host: 'http://localhost:3000',
	dashboardUrl: 'http://localhost:4200',
	jwtSecret: process.env.JWT_SECRET || "not_a_gnelf",
	db: {
		host: 'db'
	},
	auth: {
		clientId: process.env.DISCORD_CLIENT_ID,
		clientSecret: process.env.DISCORD_CLIENT_SECRET
	}
};
